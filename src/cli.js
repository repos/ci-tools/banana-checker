#!/usr/bin/env node
'use strict';

// argv: 0 = bin/node, 1 = src/cli.js, 2... = params
const params = process.argv.slice( 2 );
const dirs = [];
const options = {};
for ( const param of params ) {
	if ( !param.startsWith( '--' ) ) {
		dirs.push( param );
		continue;
	}
	// eslint-disable-next-line security/detect-unsafe-regex
	const [ , key, value ] = /([a-zA-Z]+)(?:=(.+))?/.exec( param.slice( 2 ) );
	switch ( key ) {
		// String options
		case 'sourceFile':
		case 'documentationFile':
			options[ key ] = value;
			break;

		// Boolean or 'initial'
		case 'requireLowerCase':
			if ( value === 'initial' ) {
				options[ key ] = value;
				break;
			}

			// Fall through

		// Error/warn/off (with legacy boolean values)
		// eslint-ignore-next-line no-fallthrough
		case 'disallowBlankTranslations':
		case 'disallowUnusedDocumentation':
		case 'disallowEmptyDocumentation':
		case 'requireCompleteMessageDocumentation':
		case 'requireMetadata':
		case 'disallowDuplicateTranslations':
		case 'disallowUnusedTranslations':
		case 'requireCompletelyUsedParameters':
		case 'disallowLeadingWhitespace':
		case 'disallowTrailingWhitespace':
		case 'ignoreMissingBlankTranslations': // Deprecated (mapped in src/banana.js)
		case 'allowLeadingWhitespace': // Deprecated (mapped in src/banana.js)
		case 'allowTrailingWhitespace': // Deprecated (mapped in src/banana.js)
			if ( value === '1' || value === 'true' ) {
				options[ key ] = 'error';
			} else if ( value === '0' || value === 'false' ) {
				options[ key ] = 'off';
			} else if ( value === 'error' || value === 'warn' || value === 'off' ) {
				options[ key ] = value;
			} else {
				console.error( `banana-check: Invalid option ignored, --${ key }=${ value }` );
			}
			break;

		// Boolean
		case 'autofix':
		case 'autofixTranslated':
			if ( value === '1' || value === 'true' ) {
				options[ key ] = true;
			} else if ( value === '0' || value === 'false' ) {
				options[ key ] = false;
			} else {
				console.error( `banana-check: Invalid option ignored, --${ key }=${ value }` );
			}
			break;

		// Array options
		case 'requireCompleteTranslationLanguages':
		case 'requireCompleteTranslationMessages':
		case 'requireKeyPrefix':
			options[ key ] = ( value || '' ).split( ',' );
			break;

		default:
			console.error( `banana-check: Invalid option ignored, --${ key }` );
	}
}

if ( !dirs.length ) {
	console.error( 'banana-check: Specify one or more directories.' );
	// eslint-disable-next-line n/no-process-exit
	process.exit( 1 );
}

const bananaChecker = require( '../src/banana.js' );
let result = true;
dirs.forEach( ( dir ) => {
	result = bananaChecker(
		dir,
		options,
		console.error,
		console.warn,
		true
	) && result;
} );
if ( !result ) {
	// eslint-disable-next-line n/no-process-exit
	process.exit( 1 );
}

console.log( `Checked ${ dirs.length } message director${ ( dirs.length > 1 ? 'ies' : 'y' ) }.` );
