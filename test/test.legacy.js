'use strict';

const assert = require( 'assert' );
const bananaChecker = require( '../src/banana.js' );
const utils = require( './utils.js' );

const PASS = true;
const FAIL = false;

console.log( 'test: disallowEmptyDocumentation' );
{
	const errs = [],
		result = bananaChecker(
			'test/disallowEmptyDocumentation',
			{
				disallowEmptyDocumentation: true
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);
	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'1 message is documented with a blank string:',
		'* second-message-key'
	] );
}

console.log( 'test: disallowEmptyDocumentation (disabled)' );
{
	const result = bananaChecker(
		'test/disallowEmptyDocumentation',
		{
			disallowEmptyDocumentation: false
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowUnusedDocumentation' );
{
	const errs = [],
		result = bananaChecker(
			'test/disallowUnusedDocumentation',
			{
				disallowUnusedDocumentation: true
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);
	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'1 message is documented but undefined:',
		'* fifth-message-key'
	] );
}

console.log( 'test: disallowUnusedDocumentation (disabled)' );
{
	const result = bananaChecker(
		'test/disallowUnusedDocumentation',
		{
			disallowUnusedDocumentation: false
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowBlankTranslations' );
{
	const warns = [],
		result = bananaChecker(
			'test/disallowBlankTranslations',
			{
				disallowBlankTranslations: true
			},
			() => {},
			( warn ) => {
				warns.push( warn );
			}
		);
	assert.strictEqual( result, PASS );
	assert.deepStrictEqual( warns, [
		'The "fr" translation has 2 blank translations:',
		'* second-message-key',
		'* third-message-key'
	] );
}

console.log( 'test: disallowBlankTranslations (disabled)' );
{
	const result = bananaChecker(
		'test/disallowUnusedDocumentation',
		{
			disallowUnusedDocumentation: false
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireCompletelyUsedParameters' );
{
	const errs = [],
		result = bananaChecker(
			'test/requireCompletelyUsedParameters',
			{
				requireCompletelyUsedParameters: true
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);

	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'The "de" translation has 1 message which fails to use all parameters:',
		'* "second-message-key" fails to use the parameters: "$2", "$1".',
		'The "fr" translation has 3 messages which fail to use all parameters:',
		'* "first-message-key" fails to use the parameter "$2".',
		'* "second-message-key" fails to use the parameter "$1".',
		'* "third-message-key" fails to use the parameters: "$2", "$3", "$4".'
	] );
}

console.log( 'test: requireCompleteMessageDocumentation' );
{
	const errs = [],
		result = bananaChecker(
			'test/requireCompleteMessageDocumentation',
			{
				requireCompleteMessageDocumentation: true
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);
	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'1 message lacks documentation in qqq.json:',
		'* third-message-key'
	] );
}

console.log( 'test: requireCompleteMessageDocumentation (disabled)' );
{
	const result = bananaChecker(
		'test/requireCompleteMessageDocumentation',
		{
			requireCompleteMessageDocumentation: false
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireMetadata' );
{
	const result = bananaChecker(
		'test/requireMetadata',
		{
			requireMetadata: true
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, FAIL );
}

console.log( 'test: requireMetadata (disabled)' );
{
	const result = bananaChecker(
		'test/requireMetadata',
		{
			requireMetadata: false
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: skipIncompleteMessageDocumentation' );
{
	const result = bananaChecker(
		'test/skipIncompleteMessageDocumentation',
		{
			skipIncompleteMessageDocumentation: [
				'third-message-key',
				'fourth-message-key'
			]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireLowerCase' );
{
	const result = bananaChecker(
		'test/requireLowerCase/full',
		{
			requireLowerCase: true
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, FAIL );
}

console.log( 'test: requireLowerCase (disabled)' );
{
	// Allow any mixed case
	const result = bananaChecker(
		'test/requireLowerCase/full',
		{
			requireLowerCase: false
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireLowerCase=initial' );
{
	// Allow mixed case, but still require lowercase initial
	const result = bananaChecker(
		'test/requireLowerCase/initial',
		{
			requireLowerCase: 'initial'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireKeyPrefix (string, correct)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/single',
		{
			requireKeyPrefix: 'alice'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireKeyPrefix (string, wrong)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/single',
		{
			requireKeyPrefix: 'bob'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, FAIL );
}

console.log( 'test: requireKeyPrefix (array/single, correct)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/single',
		{
			requireKeyPrefix: [ 'alice' ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireKeyPrefix (array/multiple, correct)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/multiple',
		{
			requireKeyPrefix: [ 'alice', 'bob', 'timmy' ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: allowLeadingWhitespace (enabled)' );
{
	const result = bananaChecker(
		'test/allowLeadingWhitespace',
		{
			allowLeadingWhitespace: true
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: allowLeadingWhitespace (disabled)' );
{
	const errs = [],
		warns = [],
		result = bananaChecker(
			'test/allowLeadingWhitespace',
			{
				allowLeadingWhitespace: false
			},
			( err ) => {
				errs.push( err );
			},
			( warn ) => {
				warns.push( warn );
			}
		);

	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'3 messages have leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key',
		'3 message documentations have leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key'
	] );
	assert.deepStrictEqual( warns, [
		'The "de" translation has 2 translations with leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'The "fr" translation has 3 translations with leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key'
	] );
}

console.log( 'test: allowLeadingWhitespace (disabled, autofix)' );
{
	utils.testAutofixer(
		'test/allowLeadingWhitespace',
		'test/allowLeadingWhitespace-fixed',
		{
			autofix: true,
			allowLeadingWhitespace: false
		},
		FAIL
	);
}

console.log( 'test: allowTrailingWhitespace (enabled)' );
{
	const result = bananaChecker(
		'test/allowTrailingWhitespace',
		{
			allowTrailingWhitespace: true
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: allowTrailingWhitespace (disabled)' );
{
	const errs = [],
		warns = [],
		result = bananaChecker(
			'test/allowTrailingWhitespace',
			{
				allowTrailingWhitespace: false
			},
			( err ) => {
				errs.push( err );
			},
			( warn ) => {
				warns.push( warn );
			}
		);

	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'3 messages have trailing whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key',
		'3 message documentations have trailing whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key'
	] );
	assert.deepStrictEqual( warns, [
		'The "de" translation has 3 translations with trailing whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key',
		'The "fr" translation has 2 translations with trailing whitespace:',
		'* first-message-key',
		'* third-message-key'
	] );
}

console.log( 'test: allowTrailingWhitespace (disabled, autofix)' );
{
	utils.testAutofixer(
		'test/allowTrailingWhitespace',
		'test/allowTrailingWhitespace-fixed',
		{
			autofix: true,
			allowTrailingWhitespace: false
		},
		FAIL
	);
}
