'use strict';

require( './test.legacy' );

const assert = require( 'assert' );
const bananaChecker = require( '../src/banana.js' );
const utils = require( './utils.js' );

const PASS = true;
const FAIL = false;

console.log( 'test: simple' );
{
	const result = bananaChecker(
		'test/simple',
		{},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: declare files explicitly' );
{
	const result = bananaChecker(
		'test/simple',
		{
			sourceFile: 'en.json',
			documentationFile: 'qqq.json'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: custom file names' );
{
	const result = bananaChecker(
		'test/advanced/a',
		{
			sourceFile: 'messages.json',
			documentationFile: 'documentation.json'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: files not found' );
{
	assert.throws(
		() => {
			bananaChecker(
				'test/simple',
				{
					sourceFile: 'messages.json',
					documentationFile: 'documentation.json'
				},
				() => {},
				() => {}
			);
		},
		/Cannot find/
	);
}

console.log( 'test: disallowEmptyDocumentation' );
{
	const errs = [],
		result = bananaChecker(
			'test/disallowEmptyDocumentation',
			{
				disallowEmptyDocumentation: 'error'
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);
	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'1 message is documented with a blank string:',
		'* second-message-key'
	] );
}

console.log( 'test: disallowEmptyDocumentation (off)' );
{
	const result = bananaChecker(
		'test/disallowEmptyDocumentation',
		{
			disallowEmptyDocumentation: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowUnusedDocumentation' );
{
	const errs = [],
		result = bananaChecker(
			'test/disallowUnusedDocumentation',
			{
				disallowUnusedDocumentation: 'error'
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);
	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'1 message is documented but undefined:',
		'* fifth-message-key'
	] );
}

console.log( 'test: disallowUnusedDocumentation (off)' );
{
	const result = bananaChecker(
		'test/disallowUnusedDocumentation',
		{
			disallowUnusedDocumentation: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowBlankTranslations' );
{
	const warns = [],
		result = bananaChecker(
			'test/disallowBlankTranslations',
			{
				disallowBlankTranslations: 'warn'
			},
			() => {},
			( warn ) => {
				warns.push( warn );
			}
		);
	assert.strictEqual( result, PASS );
	assert.deepStrictEqual( warns, [
		'The "fr" translation has 2 blank translations:',
		'* second-message-key',
		'* third-message-key'
	] );
}

console.log( 'test: disallowBlankTranslations (off)' );
{
	const result = bananaChecker(
		'test/disallowUnusedDocumentation',
		{
			disallowUnusedDocumentation: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireCompletelyUsedParameters' );
{
	const errs = [],
		result = bananaChecker(
			'test/requireCompletelyUsedParameters',
			{
				requireCompletelyUsedParameters: 'error'
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);

	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'The "de" translation has 1 message which fails to use all parameters:',
		'* "second-message-key" fails to use the parameters: "$2", "$1".',
		'The "fr" translation has 3 messages which fail to use all parameters:',
		'* "first-message-key" fails to use the parameter "$2".',
		'* "second-message-key" fails to use the parameter "$1".',
		'* "third-message-key" fails to use the parameters: "$2", "$3", "$4".'
	] );
}

console.log( 'test: requireCompleteMessageDocumentation' );
{
	const errs = [],
		result = bananaChecker(
			'test/requireCompleteMessageDocumentation',
			{
				requireCompleteMessageDocumentation: 'error'
			},
			( err ) => {
				errs.push( err );
			},
			() => {}
		);
	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'1 message lacks documentation in qqq.json:',
		'* third-message-key'
	] );
}

console.log( 'test: requireCompleteMessageDocumentation (skip)' );
{
	const result = bananaChecker(
		'test/skipIncompleteMessageDocumentation',
		{
			requireCompleteMessageDocumentation: [
				'error',
				{ skip: [ 'third-message-key', 'fourth-message-key' ] }
			]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireCompleteMessageDocumentation (off)' );
{
	const result = bananaChecker(
		'test/requireCompleteMessageDocumentation',
		{
			requireCompleteMessageDocumentation: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireMetadata' );
{
	const result = bananaChecker(
		'test/requireMetadata',
		{
			requireMetadata: 'error'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, FAIL );
}

console.log( 'test: requireMetadata (off)' );
{
	const result = bananaChecker(
		'test/requireMetadata',
		{
			requireMetadata: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireLowerCase' );
{
	const result = bananaChecker(
		'test/requireLowerCase/full',
		{
			requireLowerCase: 'error'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, FAIL );
}

console.log( 'test: requireLowerCase (off)' );
{
	// Allow any mixed case
	const result = bananaChecker(
		'test/requireLowerCase/full',
		{
			requireLowerCase: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireLowerCase=initial' );
{
	// Allow mixed case, but still require lowercase initial
	const result = bananaChecker(
		'test/requireLowerCase/initial',
		{
			requireLowerCase: [ 'error', { initial: true } ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireKeyPrefix (string, correct)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/single',
		{
			requireKeyPrefix: [ 'error', { prefix: 'alice' } ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireKeyPrefix (string, wrong)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/single',
		{
			requireKeyPrefix: [ 'error', { prefix: 'bob' } ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, FAIL );
}

console.log( 'test: requireKeyPrefix (array/single, correct)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/single',
		{
			requireKeyPrefix: [ 'error', { prefix: [ 'alice' ] } ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: requireKeyPrefix (array/multiple, correct)' );
{
	const result = bananaChecker(
		'test/requireKeyPrefix/multiple',
		{
			requireKeyPrefix: [ 'error', { prefix: [ 'alice', 'bob', 'timmy' ] } ]
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowLeadingWhitespace/disallowLeadingWhitespaceTranslated' );
{
	const errs = [],
		warns = [],
		result = bananaChecker(
			'test/allowLeadingWhitespace',
			{
				disallowLeadingWhitespace: 'error',
				disallowLeadingWhitespaceTranslated: 'warn'
			},
			( err ) => {
				errs.push( err );
			},
			( warn ) => {
				warns.push( warn );
			}
		);

	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'3 messages have leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key',
		'3 message documentations have leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key'
	] );
	assert.deepStrictEqual( warns, [
		'The "de" translation has 2 translations with leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'The "fr" translation has 3 translations with leading whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key'
	] );
}

console.log( 'test: disallowLeadingWhitespace (off)' );
{
	const result = bananaChecker(
		'test/allowLeadingWhitespace',
		{
			disallowLeadingWhitespace: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowLeadingWhitespace (autofix)' );
{
	utils.testAutofixer(
		'test/allowLeadingWhitespace',
		'test/allowLeadingWhitespace-fixed',
		{
			autofix: true,
			disallowLeadingWhitespace: 'error'
		},
		FAIL
	);
}

console.log( 'test: disallowLeadingWhitespace (autofixTranslated)' );
{
	utils.testAutofixer(
		'test/allowLeadingWhitespace',
		'test/allowLeadingWhitespace-fixedTranslated',
		{
			autofixTranslated: true,
			disallowLeadingWhitespace: 'error'
		},
		FAIL,
		true
	);
}

console.log( 'test: disallowTrailingWhitespace/disallowTrailingWhitespaceTranslated' );
{
	const errs = [],
		warns = [],
		result = bananaChecker(
			'test/allowTrailingWhitespace',
			{
				disallowTrailingWhitespace: 'error',
				disallowTrailingWhitespaceTranslated: 'warn'
			},
			( err ) => {
				errs.push( err );
			},
			( warn ) => {
				warns.push( warn );
			}
		);

	assert.strictEqual( result, FAIL );
	assert.deepStrictEqual( errs, [
		'3 messages have trailing whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key',
		'3 message documentations have trailing whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key'
	] );
	assert.deepStrictEqual( warns, [
		'The "de" translation has 3 translations with trailing whitespace:',
		'* first-message-key',
		'* second-message-key',
		'* third-message-key',
		'The "fr" translation has 2 translations with trailing whitespace:',
		'* first-message-key',
		'* third-message-key'
	] );
}

console.log( 'test: disallowTrailingWhitespace (off)' );
{
	const result = bananaChecker(
		'test/allowTrailingWhitespace',
		{
			disallowTrailingWhitespace: 'off'
		},
		() => {},
		() => {}
	);
	assert.strictEqual( result, PASS );
}

console.log( 'test: disallowTrailingWhitespace (autofix)' );
{
	utils.testAutofixer(
		'test/allowTrailingWhitespace',
		'test/allowTrailingWhitespace-fixed',
		{
			autofix: true,
			disallowTrailingWhitespace: 'error'
		},
		FAIL
	);
}

console.log( 'test: disallowTrailingWhitespace (autofixTranslated)' );
{
	utils.testAutofixer(
		'test/allowTrailingWhitespace',
		'test/allowTrailingWhitespace-fixedTranslated',
		{
			autofixTranslated: true,
			disallowTrailingWhitespace: 'error'
		},
		FAIL
	);
}
