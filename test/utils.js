'use strict';

const assert = require( 'node:assert' );
const fs = require( 'node:fs' );
const os = require( 'node:os' );
const path = require( 'node:path' );

const bananaChecker = require( '../src/banana.js' );

const assertDirectoriesMatch = ( first, second ) => {
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const firstFiles = fs.readdirSync( first );
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const secondFiles = fs.readdirSync( second );

	assert.deepStrictEqual( firstFiles, secondFiles );

	firstFiles.forEach( ( file ) => assert.strictEqual(
		// eslint-disable-next-line security/detect-non-literal-fs-filename
		fs.readFileSync( path.join( first, file ), { encoding: 'utf8' } ),
		// eslint-disable-next-line security/detect-non-literal-fs-filename
		fs.readFileSync( path.join( second, file ), { encoding: 'utf8' } )
	) );
};

const testAutofixer = ( sourceDirectory, expectedDirectory, options, expectedResult ) => {
	const tmpDirectory = fs.mkdtempSync(
		path.join( os.tmpdir(), 'banana-test-' )
	);
	try {
		fs.cpSync( sourceDirectory, tmpDirectory, { recursive: true } );

		const result = bananaChecker(
			tmpDirectory,
			options,
			() => {},
			() => {}
		);

		assert.strictEqual( result, expectedResult );

		assertDirectoriesMatch( tmpDirectory, expectedDirectory );
	} finally {
		fs.rmSync( tmpDirectory, { recursive: true, force: true } );
	}
};

module.exports = {
	testAutofixer
};
